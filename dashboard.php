<?php
session_start(); // Starting Session
date_default_timezone_set('Asia/Dhaka');

/* * *******************************************************************
 * WARNING: This file is part of Product Online Attendance Tracker  * 
 * DO NOT edit this file, under any circumstances.                  *
 * Do all modifications in you admin panel.                         *
 *                                                                  *
 * @package Online Attendance Tracker                               *
 * @version 1.0.1                                                   *
 * @author Giribaz ® <info@giribaz.com>                             *
 * @link http://giribaz.com                                         *
 * @author Md. Ruhul Amin <info@mdruhulamin.me>                     *                                
 * @copyright Copyright © 2017 - Giribaz.                           *
 * @license GPL-2.0+                                                *
 * @license http://www.gnu.org/licenses/gpl-2.0.html                *
 * ******************************************************************* */

/**
 * @package Admin Dashboard 
 * @todo Admin Dashboard Page
 */
include 'include/config.php';
$userid = $_SESSION['userId'];
if (!isset($_SESSION['userId'])) { //if login in session is not set
    header("Location: index.php");
}

$query = "SELECT * FROM users WHERE userId = '$userid'";
$result = mysql_query($query) or die("Error!");
$row = mysql_fetch_array($result);

$officeTime = mysql_fetch_array(mysql_query("SELECT * FROM time"));
$officeTimeinmin = $officeTime['timeinMin'];
?>	
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="#">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>OAT - DASHBOARD</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- jQuery (necessary JavaScript plugins) -->
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon" />
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <!-- Animation library for notifications -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>
        <!-- Light Bootstrap Table core CSS -->
        <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
        <!-- CSS for Demo Purpose, don't include it in your project -->
        <link href="assets/css/demo.css" rel="stylesheet"/>
        <link href="assets/css/clock.css" rel="stylesheet"/>

        <!--  Time Picker  -->
        <link href="assets/css/wickedpicker.min.css" rel="stylesheet"/>

        <!-- Datatables -->
        <link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="assets/plugins/datatables/buttons.dataTables.min.css">
        <link rel="stylesheet" href="assets/plugins/datatables/buttons.bootstrap.min.css">

        <!-- Fonts and icons -->
        <link href="assets/fonts/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

        <style type="text/css">
            #clock {
                width: 20em;
                height: 20em;
                border-radius: 50%;
                border: solid 2px black;
                margin: auto;
                position: relative;
            }

            #clock #center {
                height:2em;
                width: 2em;
                border-radius:50%;
                top: 9em;
                left: 9em;
            }
        </style>

        <script type="application/javascript">
            function setSize() {
            var b = $("html, body"),
            w = b.width(),
            x = Math.floor(w / 30) - 1,
            px = (x > 15 ? 16 : x) + "px";

            $("#clock").css({"font-size": px });
            }

            $(document).ready(function () {
            setSize();
            $(window).on("resize", setSize);
            });    
        </script>
        <script type="text/javascript">
            window.onload = setInterval(clock, 1000);

            function clock()
            {
                var d = new Date();

                var date = d.getDate();

                var month = d.getMonth();
                var montharr = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBAR", "OCTOBER", "NOVEMBAR", "DECEMBER"];
                month = montharr[month];

                var year = d.getFullYear();

                var day = d.getDay();
                var dayarr = ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATDAY"];
                day = dayarr[day];

                var hour = d.getHours();
                var min = d.getMinutes();
                var sec = d.getSeconds();

                document.getElementById("date").innerHTML = day + ", " + date + " " + month + " " + year;
                document.getElementById("time").innerHTML = hour + ":" + min + ":" + sec;
            }
        </script>
    </head>
    <body>

        <div class="wrapper">
            <div class="sidebar" data-image="assets/img/sidebar.jpg">
                <div class="sidebar-wrapper">
                    <div class="logo">
                        <center><img src="assets/img/logo.png" width="80px" height="auto"></center>
                        <a href="dashboard.php" class="simple-text">
                            ONLINE ATTENDANCE TRACKER
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="active">
                            <a href="dashboard.php">
                                <i class="pe-7s-keypad"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="files/add-employee.php">
                                <i class="pe-7s-add-user"></i>
                                <p>Add Employee</p>
                            </a>
                        </li>
                        <li>
                            <a href="files/emp-list.php">
                                <i class="pe-7s-note2"></i>
                                <p>Employee List</p>
                            </a>
                        </li>
                        <li>
                            <a href="files/add-attendance.php">
                                <i class="pe-7s-timer"></i>
                                <p>Add Attendance</p>
                            </a>
                        </li>
                        <li>
                            <a href="files/attendance-chart.php">
                                <i class="pe-7s-graph3"></i>
                                <p>Attendance Log</p>
                            </a>
                        </li>
                        <li>
                            <a href="files/attedance-record.php">
                                <i class="pe-7s-id"></i>
                                <p>Attendance Record</p>
                            </a>
                        </li>
                        <li>
                            <a href="files/advance-search.php">
                                <i class="pe-7s-search"></i>
                                <p>Advance Search</p>
                            </a>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <nav class="navbar navbar-default navbar-fixed">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">OAT DASHBOARD</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="">
                                        Hi! <?php echo $row['name']; ?>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        OPTIONS
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="files/users.php"><i class="pe-7s-users"></i>&nbsp; Add New/View User</a></li>
                                        <li><a href="files/user-profile.php"><i class="pe-7s-edit"></i>&nbsp; Edit Profile</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#timeModal"><i class="pe-7s-clock"></i>&nbsp; Office Time</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="btn btn-info btn-fill pull-right" href="logout.php">
                                        LOG OUT
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div style="background-color:#DADFE1" class="card">
                                    <div class="header">
                                        <center><h4 class="title"><b>WELCOME TO OLIVINE ATTENDANCE TRACKER</b></h4></center>
                                    </div>
                                    <br>
                                    <!-- OAT Date and Time Starts -->
                                    <div class="content">
                                        <div id="clock">
                                            <div id="hour"></div>
                                            <div id="minute"></div>
                                            <div id="second"></div>
                                            <div id="center"></div>
                                        </div>
                                        <hr/>
                                        <div class="date">
                                            <p style="font-size: 30px;font-family:Roboto;" id="date"></p>
                                        </div>
                                    </div>
                                    <!-- OAT Date and Time Ends -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div style="padding-bottom:20px; background-color:#DADFE1" class="card">
                                    <div class="header">
                                        <center><h4 class="title"><b>OAT SYSTEM OPTIONS</b></h4></center>
                                        <hr/>
                                    </div>
                                    <center>
                                        <div style="margin: 5px;" class="row">
                                            <a style="margin:5px; width:47%" href="files/emp-list.php" class="btn btn-app" title="Show Existing Employee List"><i class="fa fa-th-list fa-2x"></i><h3>EMPLOYEE LIST</h3></a>
                                            <a style="margin:5px;width:47%" href="files/add-employee.php" class="btn btn-app" title="Add Employee into System"><i class="fa fa-plus-square fa-2x"></i> <span class="badge bg-green"></span><h3>ADD EMPLOYEE</h3></a>
                                        </div>
                                        <div style="margin: 5px" class="row">
                                            <a style="margin:5px; width:47%" href="files/attendance-chart.php" class="btn btn-app" title="Daily Basis Attendance Chart"><i class="fa fa-credit-card fa-2x"></i> <span class="badge bg-green"></span><h3>ATTENDANCE CHART</h3></a>
                                            <a style="margin:5px; width:47%" href="files/attedance-record.php" class="btn btn-app" title="Daily Basis Attendance Record"><i class="fa fa-bar-chart fa-2x"></i><h3>ATTENDANCE RECORD</h3></a>
                                        </div>
                                        <div style="margin: 5px" class="row">
                                            <a style="margin:5px; width:47%" href="files/advance-search.php" class="btn btn-app" title="Employee Attendance Advance Search Filtaring"><i class="fa fa-search fa-2x"></i><h3>ADVANCE SEARCH</h3></a>
                                            <a style="margin:5px; width:47%" href="#" data-toggle="tooltip" class="btn btn-app" title="Edit System User Profile"><i class="fa fa-pencil-square-o fa-2x"></i> <span class="badge bg-green"></span><h3>EDIT PROFILE</h3></a>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="header">
                                        <center><h4 class="title"><b>EMPLOYEE DAYLY BASIS ATTENDANCE CARD</b></h4></center>
                                        <hr/>
                                    </div>
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- general form elements -->
                                                <div class="box-body" id="datatablesmp">
                                                    <table id="datatable" class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Employee</th>
                                                                <th>Employee Name</th>
                                                                <th>Enter Time</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            /* Retriving Data from Employee Detail Table */
                                                            $empListquery = mysql_query("SELECT * FROM employee");

                                                            while ($empListrow = mysql_fetch_array($empListquery)) {
                                                                $today = date("d/m/Y");
                                                                $empID = $empListrow['employeeId'];
                                                                $empData = mysql_fetch_array(mysql_query("SELECT * FROM empattendance WHERE employeeId = '$empID' AND dateInnumber = '$today' AND empState = 'IN' ORDER BY serialNo ASC LIMIT 1"));
                                                                $emptimeInmin = $empData['timeInMin'];
                                                                $empIntime = $empData['time'];
                                                                ?>
                                                                <tr>
                                                                    <td><center><?php echo "<img class='img-rounded' src=\"{$empListrow['employeeImg']}\" height='40' width='40' />"; ?></center></td>		    
                                                            <td><?php echo $empListrow['empName']; ?></td>
                                                            <td><?php echo $empIntime; ?></td>
                                                            <td>
                                                            <center><?php
                                                            if ($emptimeInmin > $officeTimeinmin) {
                                                                echo "<img src=\"assets/img/red.png\" height='40' width='40'/>";
                                                            } else {
                                                                echo "<img src=\"assets/img/green.png\" height='40' width='40'/>";
                                                            }
                                                                ?></center>
                                                            </td>			
                                                        <?php } ?>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div><!-- /.box-body -->
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Time Modal -->
        <div class="modal fade" id="timeModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">OFFICE TIME CARD</h4>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="header">
                                <center><h2 class="title"><b>CURRENT OFFICE TIME</b></h2></center>
                                <hr/>
                            </div>
                            <div class="col-md-12">
                                <center><h2><b><?php echo $officeTime['officeTime']; ?></b></h2></center>
                                <center><h5><b>Last Modification Date : <?php echo $officeTime['date']; ?></b></h4></center>
                                <hr/> 
                            </div>
                        </div>
                        <div class="row">
                            <form action="include/query.php" method="post">
                                <div class="col-md-2"></div>           
                                <div class="col-md-6">   
                                    <div class="form-group">
                                        <label>TIME</label>
                                        <div class="input-group" >
                                            <input id="timepicker1" type="text" class="form-control" name="officeTime"/>
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div style="margin-top:25px" class="form-group">
                                        <button type="submit" name="setNewtimereq" class="btn btn-info btn-fill">SET NEW TIME</button>
                                    </div>
                                </div> 
                            </form>
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!--  Checkbox, Radio & Switch Plugins -->
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

    <!--  Charts Plugin -->
    <script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!-- DataTables -->
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="assets/plugins/datatables/buttons.bootstrap.min.js"></script>
    <script src="assets/plugins/datatables/jszip.min.js"></script>
    <script src="assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="assets/plugins/datatables/buttons.colVis.min.js"></script>
    <script src="assets/plugins/datatables/buttons.print.min.js"></script>

    <!-- Time Picker  -->
    <script src="assets/js/wickedpicker.min.js"></script>
    <script>
            $('#timepicker1').timepicki({
                show_meridian: false,
                min_hour_value: 0,
                max_hour_value: 23,
                step_size_minutes: 15,
                overflow_minutes: true,
                increase_direction: 'up',
                disable_keyboard_mobile: true});
    </script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Clock JS -->
    <script src="assets/js/light-bootstrap-dashboard.js"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/clock.js"></script>
    <script>
            (function ($) {
                $(Phhht.Clock.setUp);
            }(jQuery));
    </script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <script type="text/javascript">
            $(document).ready(function () {

                demo.initChartist();

            });
    </script>
    <script>
        $(function () {
            var table = $('#datatable').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": true,
                "scrollX": true,
                buttons: ['', '', '']
            });

            table.buttons().container()
                    .appendTo('#datatablesmp');

        });
    </script> 

</html>
