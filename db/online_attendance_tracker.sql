-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2017 at 10:58 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_attendance_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `empattendance`
--

CREATE TABLE `empattendance` (
  `serialNo` int(11) NOT NULL,
  `employeeId` varchar(100) NOT NULL,
  `date` varchar(50) NOT NULL,
  `day` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  `empState` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empattendance`
--

INSERT INTO `empattendance` (`serialNo`, `employeeId`, `date`, `day`, `time`, `empState`) VALUES
(2, '141211-10', '15/04/2016', 'Tuesday', '12.10', 'IN'),
(3, '141211-10', '15/04/2016', 'Tuesday', '12.10', 'OUT'),
(4, '141211-10', '15/04/2016', 'Tuesday', '14.15', 'IN'),
(5, '141211-10', '15/04/2016', 'Tuesday', '15.15', 'OUT'),
(6, '141211-11', '15/04/2016', 'Tuesday', '12.10', 'IN'),
(7, '141211-11', '15/04/2016', 'Tuesday', '12.10', 'OUT'),
(8, '141211-11', '15/04/2016', 'Tuesday', '14.15', 'IN'),
(9, '141211-11', '15/04/2016', 'Tuesday', '15.15', 'OUT'),
(10, '141211-08', '15/04/2016', 'Friday', '9.10', 'IN'),
(11, '141211-08', '15/04/2016', 'Friday', '11.00', 'OUT'),
(12, '141211-08', '15/04/2016', 'Friday', '11.30', 'IN'),
(13, '141211-08', '15/04/2016', 'Friday', '13.00', 'OUT'),
(16, '141211-08', '15/04/2016', 'Friday', '14.15', 'IN'),
(17, '141211-08', '15/04/2016', 'Friday', '18.30', 'OUT'),
(24, '141211-13', '19/04/2016', 'Tuesday', '18.0:12.0', 'IN'),
(25, '141211-13', '19/04/2016', 'Tuesday', '06:13:12 PM', 'OUT'),
(26, '141211-06', '19/04/2016', 'Tuesday', '06:14:08 PM', 'IN'),
(27, '141211-06', '19/04/2016', 'Tuesday', '06:41:54 PM', 'OUT'),
(30, '20170102', '02/08/2017', 'Wednesday', '12:5', 'Out'),
(31, '20170102', '02/08/2017', 'Wednesday', '12:4', 'In');

-- --------------------------------------------------------

--
-- Table structure for table `empattendancerecord`
--

CREATE TABLE `empattendancerecord` (
  `serialNo` int(11) NOT NULL,
  `employeeId` varchar(30) NOT NULL,
  `empName` varchar(50) NOT NULL,
  `employeeDesignation` varchar(30) NOT NULL,
  `department` varchar(20) NOT NULL,
  `inTime` varchar(20) NOT NULL,
  `outTime` varchar(20) NOT NULL,
  `totalofficehour` varchar(30) NOT NULL,
  `totalworkhour` varchar(30) NOT NULL,
  `resttime` varchar(30) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empattendancerecord`
--

INSERT INTO `empattendancerecord` (`serialNo`, `employeeId`, `empName`, `employeeDesignation`, `department`, `inTime`, `outTime`, `totalofficehour`, `totalworkhour`, `resttime`, `date`) VALUES
(22, '141211-10', 'Palash', 'Web Developer', 'Software Development', '12.10', '15.15', '3.05', '1', '2.05', '05/04/2016'),
(23, '141211-10', 'Palash', 'Web Developer', 'Software Development', '12.10', '15.15', '3.05', '1', '2.05', '15/04/2016'),
(24, '141211-08', 'Ruhul', 'Jr. Web Developer', 'Software Development', '9.10', '18.30', '9.2', '7.75', '1.45', '15/04/2016'),
(25, '141211-13', 'Nusrat', 'Creative Content Writer', 'Software Development', '18.0:12.0', '06:13:12 PM', '0.02', '0.02', '0', '19/04/2016'),
(26, '141211-06', 'Rhythm', 'Sr. Software Developer', 'Software Development', '06:14:08 PM', '06:41:54 PM', '0.45', '0.45', '0', '19/04/2016');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employeeId` varchar(100) NOT NULL,
  `empName` varchar(100) NOT NULL,
  `empEmail` varchar(100) NOT NULL,
  `empPhoneNumber` varchar(100) NOT NULL,
  `employeeDesignation` varchar(100) NOT NULL,
  `employeeImg` varchar(200) NOT NULL DEFAULT 'lib/images/avatar.png',
  `department` varchar(100) NOT NULL,
  `employeeType` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employeeId`, `empName`, `empEmail`, `empPhoneNumber`, `employeeDesignation`, `employeeImg`, `department`, `employeeType`) VALUES
('20170102', 'Sagor', 'sagor@gmail.com', '98273923', 'Dev', '../assets/members/20170102.jpg', 'PHP', 'Full'),
('20170103', 'Ridoy', 'hridoy@gmail.com', '98273923323', 'manager', '../assets/members/20170103.jpg', 'development', 'Full');

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE `time` (
  `serialNo` int(11) NOT NULL,
  `officeTime` varchar(20) NOT NULL,
  `timeinMin` varchar(20) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time`
--

INSERT INTO `time` (`serialNo`, `officeTime`, `timeinMin`, `date`) VALUES
(1, '09 : 45', '585', '15/04/2016');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userId` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `userType` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userId`, `name`, `phone`, `email`, `password`, `userType`, `status`) VALUES
(7, 'Admin', 'Ridoy', '01928623875', 'giribaz@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 'Admin', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empattendance`
--
ALTER TABLE `empattendance`
  ADD PRIMARY KEY (`serialNo`);

--
-- Indexes for table `empattendancerecord`
--
ALTER TABLE `empattendancerecord`
  ADD PRIMARY KEY (`serialNo`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employeeId`);

--
-- Indexes for table `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`serialNo`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `empattendance`
--
ALTER TABLE `empattendance`
  MODIFY `serialNo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `empattendancerecord`
--
ALTER TABLE `empattendancerecord`
  MODIFY `serialNo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `time`
--
ALTER TABLE `time`
  MODIFY `serialNo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
