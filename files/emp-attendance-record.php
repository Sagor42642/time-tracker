<?php

include '../include/config.php';
include('../assets/MPDF57/mpdf.php');

/* Retriving Data from Employee Attandance Detail Table */
$empAttenrecord = mysql_query("SELECT * FROM empattendancerecord");

$mpdf = new mPDF('c', 'A4', '', '', 0, 0, 0, 0, 0, 0);

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list

$header .='
<!DOCTYPE html>
<html>
<head>
    <title>Print Invoice</title>
    <style>
        *{
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family:Arial;
            font-size:10pt;
            margin:0;
            padding:0;
        }
         
        p
        {
            margin:0;
            padding:0;
        }
         
        #wrapper
        {
            width:180mm;
            margin:0 15mm;
        }
         
        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }
 
        table
        {
            border: 1px solid #ccc;
            border-spacing:0;
            border-collapse: collapse; 
             
        }
        table td 
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            text-align:center;
            padding: 2px 5px;
        }
        
         table th 
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
            border-top: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding: 2px 5px;
        }
         
         
        table.heading
        {
            height:10px;
        }
         
        h1.heading
        {
            font-size: 30px;
            text-align: center;
            color:#000 bold;
            font-weight:normal;
        }
         
        h2.heading
        {
            font-size:20px;
            text-align: center;
            color:#000;
            font-weight:normal;
        }
        
        h3.heading
        {
            font-size:15pt;
            text-align: center;
            color:#000;
            font-weight:normal;
            text-decoration: underline;
        }
         
        hr
        {
            color:#ccc;
            background:#ccc;
        }

        .cash_bottom{
            width: 100%;
        }
        .cash_content_left{
            float: left;
            width: 70%;
        }
        .cash_content_right{
            float: left;
            width: 20%;
            text-align: center;
        }
         
        #invoice_body
        {
            height: 149mm;
        }
         
        #invoice_body , #invoice_total
        {   
            width:100%;
        }
        #invoice_body table , #invoice_total table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
            border-spacing:0;
            border-collapse: collapse; 
            margin-top:5mm;
        }
         
        #invoice_body table td , #invoice_total table td
        {
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
            padding:2mm 0;
        }
         
        #invoice_body table td.mono , #invoice_total table td.mono
        {
            font-family:monospace;
            text-align:right;
            padding-right:3mm;
            font-size:10pt;
        }
         
        #footer
        {   
            width:100%;
            margin:0 90mm;
            padding-bottom:3mm;
        }
        #footer table
        {
            width:100%;
            border-left: 1px solid #ccc;
            border-top: 1px solid #ccc;
             
            background:#eee;
             
            border-spacing:0;
            border-collapse: collapse; 
        }
        #footer table td
        {
            width:25%;
            text-align:center;
            font-size:9pt;
            border-right: 1px solid #ccc;
            border-bottom: 1px solid #ccc;
        }
    </style>
</head>
<body>
<div id="wrapper">
<br><br><br><br>
    <div>
    <table class="heading" style="width:100%;">
        <tr style="border:0px">
            <td style="width:20mm;border:0px"><img style="float:left; width:96px;height:auto;" src="../assets/img/logo.png"></td>
            <td style="width:80mm;border:0px">
              <center>
                <h1 class="heading">OLIVINE LIMITED</h1>
                <h3 class="heading">Innovative Ideas, Lead by Adroit</h3>
                <h2 class="heading">Employee Attendance Report</h2>
              </center>
            </td>
        </tr>
        <br/>
    </table>
    <br><br>
    <div id="content">
       <table style="width:100%;">
        <thead>
          <tr style="border:2px solid black;">
           <th>Date</th>
           <th>Employee ID</th>
           <th>Name</th>
           <th>Designation</th>
           <th>Total Office Hour</th>
           <th>Total Work Hour</th>
           <th>Rest Time</th>
          </tr>
        </thead>
        <tbody>';

while ($list = mysql_fetch_array($empAttenrecord)) {
    $header .= '<tr>';
    $header .= '<td>' . $list['date'] . '</td>';
    $header .= '<td>' . $list['employeeId'] . '</td>';
    $header .= '<td>' . $list['empFirstName'] . ' ' . $list['empLastName'] . '</td>';
    $header .= '<td>' . $list['employeeDesignation'] . '</td>';
    $header .= '<td>' . $list['totalofficehour'] . '</td>';
    $header .= '<td>' . $list['totalworkhour'] . '</td>';
    $header .= '<td>' . $list['resttime'] . '</td>';
    $header .= '</tr>';
}

$header .='</tbody>
       </table>
    </div>
    <br/><br/>
</div>
    
</body>
</html>';

$mpdf->WriteHTML($header);

$mpdf->Output();
?>